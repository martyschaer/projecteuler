package dev.schaer.euler

/**
 * Find the sum of all the primes below two million
 */
fun main() {
    println((2..2_000_000L).filter { isPrime(it) }.sum())
}