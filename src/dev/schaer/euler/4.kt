package dev.schaer.euler

/**
 * Find the largest palindromic product of two 3 digit numbers.
 */
fun main() {
    println((999 downTo 99).flatMap { (999 downTo 99).map { n -> it * n } }.filter { isPalindromic(it) }.max())
}

private fun isPalindromic(n: Int): Boolean {
    val number = n.toString()
    val lastIndex = number.length - 1
    for (i in (0..lastIndex)) {
        val j = lastIndex - i
        if (number[i] != number[j]) {
            return false
        }
        if (i == j) {
            return true
        }
    }
    return true
}