package dev.schaer.euler

/**
 * Determine the first triangle number with over 500 divisors.
 * ---
 * Extremely inefficient and slow. Takes around 25 minutes.
 */

fun main() {
    val start = System.currentTimeMillis()
    println(generateSequence(1L) { it + 1 }
        .map { it * (it + 1) / 2 }
        .filter {
            (1L..(it / 2L)).filter { divisor -> it % divisor == 0L }.count() >= 500
        }.first())
    val stop = System.currentTimeMillis()

    println("Took ${(stop - start) / 1000} seconds")
}
