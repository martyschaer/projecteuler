package dev.schaer.euler

/**
 * A pythagorean triplet is a set of 3 natural numbers, a < b < c which a*a + b*b == c*c
 */
fun main() {
    val target = 1000
    for (a in (1..target)) {
        for (b in ((a + 1)..target)) {
            for (c in ((b + 1)..target)) {
                if (a + b + c == target && a * a + b * b == c * c) {
                    println(a * b * c)
                }
            }
        }
    }
}