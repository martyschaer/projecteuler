package dev.schaer.euler

/**
 * Find the sum of all multiples of 3 or 5 below 1000.
 */
fun main() {
    println((0 until 1000).filter { it % 3 == 0 || it % 5 == 0 }.sum())
}