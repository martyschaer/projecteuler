package dev.schaer.euler

/**
 * What is the 10'001st prime number.
 */
fun main() {
    println(generateSequence(1L) { it + 1 }.filter { isPrime(it) }.take(10001).last())
}