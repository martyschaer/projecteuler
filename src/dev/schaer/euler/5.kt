package dev.schaer.euler

/**
 * Find the smallest positive number that's evenly divisible by (1..20)
 */
fun main() {
    println(generateSequence(20) { it + 2 }.filter { (10..20).all { divisor -> it % divisor == 0 } }.first())
}