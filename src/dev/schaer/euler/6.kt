package dev.schaer.euler

fun main() {
    val sum = (1..100).sum()
    val squareOfSum = sum * sum
    val sumOfSquares = (1..100).map { it * it }.sum()
    println(squareOfSum - sumOfSquares)
}