package dev.schaer.euler

import kotlin.math.sqrt

fun isPrime(n: Long): Boolean {
    if (n == 1L) {
        return false
    }
    for (i in (2..sqrt(n.toDouble()).toLong())) {
        if (n % i == 0L) {
            return false
        }
    }
    return true
}
