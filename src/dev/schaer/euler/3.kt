package dev.schaer.euler

import kotlin.math.sqrt

/**
 * Find the largest prime factor of the number 600851475143.
 */
fun main() {
    val toFactor = 600851475143L
    println(largestFactor(toFactor))
}

private fun largestFactor(n: Long): Long {
    if (isPrime(n)) {
        return n
    }
    for (i in (2..sqrt(n.toDouble()).toLong())) {
        if (n % i == 0L) {
            return largestFactor(n / i)
        }
    }
    throw IllegalStateException("Unable to factor $n")
}
