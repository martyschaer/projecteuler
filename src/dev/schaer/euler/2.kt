package dev.schaer.euler

/**
 * Considering the terms in the fibonacci sequence whose values are < four million,
 * find the sum of the even valued terms.
 */
fun main() {
    val cutOff = 4_000_000
    println(fibonacci().filter { it % 2 == 0 }.takeWhile { it < cutOff }.sum())
}

private fun fibonacci(): Sequence<Int> {
    return generateSequence(Pair(1, 2), { Pair(it.second, it.first + it.second) }).map { it.first }
}